<?php
	require_once( 'lib/db.php' ) ;

	$mode = $argv[ 1 ] ;
	$modes = [ 'up' , 'down' ] ;

	if ( ! in_array( $mode , $modes ) ) {
		throw new \Exception( 'Достпуны только опции: ' . implode( ', ' , $modes ) ) ;
	}

	$sql_data = file_get_contents( "migration/{$mode}.sql" ) ;

	$dbh->exec( $sql_data ) ;

	echo 'DONE' . PHP_EOL ;