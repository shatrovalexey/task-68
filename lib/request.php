<?php
	/**
	* @var mixed $request - запрос
	*/
	global $request ;

	switch ( $_SERVER[ 'REQUEST_METHOD' ] ) {
		case 'PUT' :
		case 'POST' : {
			$request = @json_decode( file_get_contents( 'php://input' ) , true ) ;

			break ;
		}
		default : {
			$request = &$_REQUEST ;

			break ;
		}
	}