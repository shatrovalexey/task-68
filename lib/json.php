<?php
	/**
	* Вывод в формате JSON
	*
	* @param mixed $data - данные для вывода
	*/
	function json_output( $data ) {
		header( 'Content-Type: application/json' ) ;

		echo json_encode( $data ) ;
		exit( 0 ) ;
	}