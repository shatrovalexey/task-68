CREATE TABLE IF NOT EXISTS `contact`(
	`id` BIGINT( 22 ) UNSIGNED NOT null AUTO_INCREMENT COMMENT 'идентификатор контакта' ,
	`phone` CHAR( 10 ) NOT null COMMENT 'номер телефона' ,
	`name` VARCHAR( 60 ) NOT null COMMENT 'имя' ,
	`email` VARCHAR( 60 ) NOT null COMMENT 'e-mail' ,

	PRIMARY KEY( `id` ) ,
	UNIQUE( `phone` )
) COMMENT 'контакт' ;

CREATE TABLE IF NOT EXISTS `source_contact`(
	`source_id` BIGINT( 22 ) UNSIGNED NOT null COMMENT 'идентификатор источника' ,
	`contact_id` BIGINT( 22 ) UNSIGNED NOT null COMMENT 'идентификатор контакта' ,
	`created_date` DATE NOT null COMMENT 'дата создания записи' ,

	PRIMARY KEY( `source_id` , `contact_id` , `created_date` ) ,
	INDEX( `contact_id` ) ,
	INDEX( `created_date` )
) COMMENT 'источник' ;