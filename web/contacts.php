<?php
require_once( '../lib/db.php' ) ;
require_once( '../lib/request.php' ) ;
require_once( '../lib/json.php' ) ;

/**
* если запрашивался номер телефона, то загружается ../src/contacts-get.php
* обратно - ../src/contacts-get.php
*/
if ( empty( $request[ 'phone' ] ) ) {
	require( '../src/contacts-post.php' ) ;
} else {
	require( '../src/contacts-get.php' ) ;
}

