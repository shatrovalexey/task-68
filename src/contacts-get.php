<?php
$sth_sel_contact = $dbh->prepare( "
SELECT
	`sc1`.`source_id` ,
	`c2`.`phone` ,
	`c2`.`name` ,
	`c2`.`email`
FROM
	`contact` AS `c1`

	INNER JOIN `source_contact` AS `sc1` ON
	( `c1`.`id` = `sc1`.`contact_id` )

	INNER JOIN `source_contact` AS `sc2` ON
	( `sc1`.`source_id` = `sc1`.`source_id` )

	INNER JOIN `contact` AS `c2` ON
	( `c2`.`id` = `sc2`.`contact_id` )
WHERE
	( `c1`.`phone` = :phone )
GROUP BY
	1 , 2 , 3 , 4
ORDER BY
	1 ASC ;
" ) ;
$sth_sel_contact->execute( [
	':phone' => $request[ 'phone' ] ,
] ) ;
$result = [ ] ;

while ( $contact = $sth_sel_contact->fetch( \PDO::FETCH_ASSOC ) ) {
	$source_id = $contact[ 'source_id' ] ;

	unset( $contact[ 'source_id' ] ) ;

	if ( empty( $result[ $source_id ] ) ) {
		$result[ $source_id ] = [ ] ;
	}

	$result[ $source_id ][] = $contact ;
}

$sth_sel_contact->closeCursor( ) ;

json_output( $result ) ;