<?php
/**
* @author Шатров Алексей Сергеевич <mail@ashatrov.ru>
*	1.1 Добавлять данные в базу массивом, например:
*		POST /contacts
*		{
*			source_id: 1,
*			items: [{
*				"name": "Анна",
*				"phone": 9001234453,
*				"email": "mail1@gmail.com"
*			}, {
*				"name": "Иван",
*				"phone": "+79001234123",
*				"email": "mail2@gmail.com"
*			}]
*		}
*
*		- `source_id` - id источника контактов, для примера достаточно 1 и 2
*		- `phone` в бд сохраняем в формате без +7 (10 цифр)
*		- `phone` не уникально, но может добавляться максимум 1 раз в сутки для каждого `source_id`
*
*		В ответе - количество добавленных контактов.
*/

/**
* @var integer $result - результат
*/
$result = 0 ;

/**
* @var resource $sth_sel_contact - поиск контакта в БД
*/
$sth_sel_contact = $dbh->prepare( "
SELECT
	`c1`.`id`
FROM
	`contact` AS `c1`
WHERE
	( `c1`.`phone` = :phone )
LIMIT 1 ;
" ) ;

/**
* @var resource $sth_ins_contact - создание контакта в БД
*/
$sth_ins_contact = $dbh->prepare( "
INSERT INTO
	`contact`
SET
	`name` := :name ,
	`phone` := :phone ,
	`email` := :email ;
" ) ;

/**
* @var resource $sth_ins_source_contact - создание связи source и контакт
*/
$sth_ins_source_contact = $dbh->prepare( "
INSERT IGNORE INTO
	`source_contact`
SET
	`source_id` := :source_id ,
	`contact_id` := :contact_id ,
	`created_date` := current_date( ) ;
" ) ;

/**
* @var array $item - просмотр контактов
*/

foreach ( $request[ 'items' ] as &$item ) {
	$item[ 'phone' ] = preg_replace( '{\D+}s' , '' , $item[ 'phone' ] ) ;

	if ( strlen( $item[ 'phone' ] ) < 10 ) {
		continue ;
	} elseif ( strlen( $item[ 'phone' ] ) > 11 ) {
		continue ;
	} elseif ( strlen( $item[ 'phone' ] ) == 11 ) {
		$item[ 'phone' ] = substr( $item[ 'phone' ] , 1 ) ;
	}

	$sth_sel_contact->execute( [
		':phone' => $item[ 'phone' ] ,
	] ) ;
	list( $item[ 'id' ] ) = $sth_sel_contact->fetch( \PDO::FETCH_NUM ) ;

	if ( empty( $item[ 'id' ] ) ) {
		$sth_ins_contact->execute( [
			':phone' => $item[ 'phone' ] ,
			':name' => $item[ 'name' ] ,
			':email' => $item[ 'email' ] ,
		] ) ;

		$item[ 'id' ] = $dbh->lastInsertId( ) ;
	}

	print_r( [
		':source_id' =>  $request[ 'source_id' ] ,
		':contact_id' => $item[ 'id' ] ,
	] ) ; echo PHP_EOL ;

	$result += $sth_ins_source_contact->execute( [
		':source_id' =>  $request[ 'source_id' ] ,
		':contact_id' => $item[ 'id' ] ,
	] ) ;
}

$sth_ins_source_contact->closeCursor( ) ;
$sth_sel_contact->closeCursor( ) ;
$sth_ins_contact->closeCursor( ) ;

json_output( $result ) ;